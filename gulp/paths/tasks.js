'use strict'

module.exports = [
	'./gulp/tasks/sass.js',
	'./gulp/tasks/pug.js',
	'./gulp/tasks/serve.js',
	'./gulp/tasks/watch.js',
	'./gulp/tasks/clean.js',
	'./gulp/tasks/wiredep.js',
	'./gulp/tasks/useref.js',
	'./gulp/tasks/sprite.js',
	'./gulp/tasks/fonts.js',
	'./gulp/tasks/spriteSVG.js'
];