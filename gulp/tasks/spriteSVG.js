'use strict';
	
module.exports = function() {
    $.gulp.task('svgSpriteBuild', function () {
	return $.gulp.src('src/assets/sprite/SVG/*.svg')
	// minify svg
		.pipe($.gp.svgmin({
			js2svg: {
				pretty: true
			}
		}))
		// remove all fill, style and stroke declarations in out shapes
		.pipe($.gp.cheerio({
			run: function ($) {
				$('[fill]').removeAttr('fill');
				$('[stroke]').removeAttr('stroke');
				$('[style]').removeAttr('style');
			},
			parserOptions: {xmlMode: true}
		}))
		// cheerio plugin create unnecessary string '&gt;', so replace it.
		.pipe($.gp.replace('&gt;', '>'))
		// build svg sprite
		.pipe($.gp.svgSprite({
			mode: {
				symbol: {
					sprite: "../assets/spriteSVG.svg",
					render: {
						scss: {
							dest:'../../src/styles/_misc/spriteSVG.scss'
							// template: "src/styles/misc/spriteSVG_template.scss"
						}
					}
				}
			}
		}))
		.pipe($.gulp.dest('build/'));
});
};




// module.exports = function() {
//     $.gulp.task('svgSpriteBuild', function () {
//         return $.gulp.src('src/assets/icons/*.svg')
//     .pipe($.gp.svgSymbols())
//     .pipe($.gulp.dest('build/assets'));
// });





    // $.gulp.task('svgSpriteBuild', function(){
    //     return $.gulp.src('./src/assets/icons/*.svg')
    //     .pipe($.gp.svgSprite({
    //             mode: {
    //                 symbol: {
    //                     sprite: "assets/sprite.svg",
    //                     render: {
    //                         css: {
    //                             dest:'./css/svgSprite'
    //                         }
    //                     }
    //                 }
    //             }
    //         }))
    // .pipe($.gulp.dest('build'));
    // });





// module.exports = function() {
//     $.gulp.task('svgSpriteBuild', function () {
//         return $.gulp.src('./src/assets/icons/*.svg')
//         // minify svg
//             .pipe($.gp.svgmin({
//                 js2svg: {
//                     pretty: true
//                 }
//             }))
//             // remove all fill, style and stroke declarations in out shapes
//             .pipe($.gp.cheerio({
//                 run: function ($) {
//                     $('[fill]').removeAttr('fill');
//                     $('[stroke]').removeAttr('stroke');
//                     $('[style]').removeAttr('style');
//                 },
//                 parserOptions: {xmlMode: true}
//             }))
//             // cheerio plugin create unnecessary string '&gt;', so replace it.
//             .pipe($.gp.replace('&gt;', '>'))
//             // build svg sprite
//             .pipe($.gp.svgSprite({
//                 mode: {
//                     symbol: {
//                         sprite: "../sprite.svg",
//                         render: {
//                             scss: {
//                                 dest:'./src/'
//                             }
//                         }
//                     }
//                 }
//             }))
//             .pipe($.gulp.dest('./build/assets/'));
//     });
// };